
import './App.css';
import Title from "./Screens/Title/";
import Contacts from "./Screens/Contacts/";


const App = () => {
  return (
    <div>
        <Title/>
        <Contacts/>
    </div>
  );
}

export default App;
