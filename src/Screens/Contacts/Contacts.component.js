import React from 'react';
import {Checkbox, TextField} from "@material-ui/core";


const ContactsComponent = ({error, handleTextChange, filteredList, handleCheckChange}) => {

    const mapList = (list) => Object.entries(list).map(([key, value]) => {
        const id = value['id']
        const avatar = value['avatar'];
        const firstName = value['first_name'];
        const lastName = value['last_name']
        const contact = {
            id,
            avatar,
            firstName,
            lastName
        }
        return(
            <div>
                <li><img src={avatar} alt={'avatar'}/> {firstName} {lastName} <Checkbox onChange={e => handleCheckChange(contact)}/></li>
            </div>
        )
    })

    const mappedFilteredList = mapList(filteredList);

    return (
<div>
    <TextField placeholder={'Look up contact'} onChange={handleTextChange}></TextField>
    {mappedFilteredList}
</div>
    );
};

export default ContactsComponent;