import React, { useState, useEffect } from 'react';
import ContactsComponent from './Contacts.component';
import axios from "axios";


const Contacts = () => {

    useEffect(() => {
         const fetchData = async () => {
             try{
                 const result = await axios(
                     'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json',
                 );

                 const contactsAlphabeticalOrder = sortContactsAlphabetically(result.data);
                 setError(null);
                 setFullList(contactsAlphabeticalOrder);
                 setFilteredList(contactsAlphabeticalOrder);
             } catch (err){
                 setError('Something went wrong :/ Try again later!')
             }
        }
        fetchData();
    }, []);

    const [fullList, setFullList] = useState('');
    const [filteredList, setFilteredList] = useState('');
    const [error, setError] = useState(null);
    const [checkList, setChecklist] = useState([]);

    const sortContactsAlphabetically = (users) => {
        return users.sort((user1, user2) => user1['last_name'].localeCompare(user2['last_name']));
    };

    const handleTextChange = (e) => {
        const newFilteredList = fullList.filter((person) => person['first_name'].concat(' ', person['last_name']).toLowerCase().includes(e.target.value.toLowerCase()));
        setFilteredList(newFilteredList);
    };

    const handleCheckChange = (contact) => {

        if(checkList.some(person => person.id === contact.id)){
            setChecklist(checkList.filter(person => person.id !== contact.id))
        } else {
            setChecklist([...checkList, contact]);
        }
    }

    console.log(checkList)

    return <ContactsComponent
        error={error}
        handleTextChange={handleTextChange}
        filteredList={filteredList}
        handleCheckChange={handleCheckChange}
    />;
};

export default Contacts;