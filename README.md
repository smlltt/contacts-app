Based on the Create react app

1. Instructions on how to run the project locally: you only need to install node_modules.

2. General file structure:

Whenever a component involves logic, it is included in a folder and split in two parts:

ComponentNameFolder -> componentName.component.js + index.js

componentName.component.js is meant for rendering.
index.js contains the logic of the component.

This way logic and rendering are kept separated.

3. checkboxes

checkboxes are console-logged correctly, but they are not displayed correctly 
(they are until the filter is applied). The reason is that I am not keeping the 'checked: true/false' 
info in the state. With more time, I would have corrected the issue.

4. Style

I did not have the time to style the project. Normally I would have used material-ui.